#!/usr/bin/bash
# SPDX-License-Identifier: GPL-2.0

# This script adds a "Bugzilla: https://bugzilla.redhat.com/123456" line
# to each commit in a branch.  It takes two arguments; the bugzilla number and the
# target branch.
#
# This script requires numpatches from the kernel-tools tree, and has MINIMAL error
# checking.
#
# For example, to insert a BZ URL for 123456 on all patches in the current branch
# that targets 8.2,
#
#	./insert_bugzilla_URL 123456 8.2

if [ $# -ne 2 ]; then
	echo "$0: Specify a bugzilla and target branch."
	exit 1
fi

BUGZILLA=$1
TARGET=$2
DIRNAME=$(dirname $0)
NUMPATCHES=$(${DIRNAME}/numpatches "$TARGET")

isanumber='^[0-9]+$'
if ! [[ $NUMPATCHES =~ $isanumber ]] ; then
	echo "$0: Specify a bugzilla and target branch." >&2; exit 1
fi

[ $NUMPATCHES -eq 0 ] && echo "No patches found.  Did you forget to commit?" && exit 0

git filter-branch -f --msg-filter "sed -e '3i\Bugzilla: http://bugzilla.redhat.com/'${BUGZILLA}'\n'" HEAD~${NUMPATCHES}..HEAD >& /dev/null
