#!/bin/bash

print_usage() {
	echo "Usage: patches-report [options] [ignored_path [...]]"
	echo "List the files that are touched by each patch in the quilt series"
	echo "Options:"
	echo "  -nl | --no-list   Don't list files, only print count of files"
	echo "  -i  | --inverse   Print also inverse list: list of patches that touch each file (slow)"
	echo "Ignored_paths are not printed in the report. Hint: ignoring the paths you want to modify"\
	     "you will see a report of other files you're also modifying, maybe unintentionally."
	echo "Required packages: quilt"
}

# parse args
ignored_paths=()
while [ $# -gt 0 ]; do
	case "$1" in
		-h|--help)
			print_usage
			exit 0
			;;
		-nl|--no-list)
			LIST_FILES=false
			;;
		-i|--inverse)
			INVERSE=true
			;;
		*)
			ignored_paths+=("$(readlink -m "$1")")
			;;
	esac
	shift
done

# default values
LIST_FILES=${LIST_FILES:-true}
INVERSE=${INVERSE:-false}

# dependencies
if [[ ! -x "$(command -v quilt)" ]]; then
	echo "Error: quilt is not installed" >&2
	exit 1
fi

# helper functions
is_ignored_path() { # $1=file
	local path
	local file="$(readlink -m "$1")"
	for path in "${ignored_paths[@]}"; do
		[[ $file == $path* ]] && return 0
	done
	return 1
}

# print patches and the files that they touch
echo "BY PATCH:"
for patch in $(quilt series); do
	# get first and last patch in the series
	[ -z $first_patch ] && first_patch="$patch"
	last_patch="$patch"

	# print files touched by patches
	patch_printed=false
	count=0
	for file in $(quilt files "$patch"); do
		if ! is_ignored_path "$file"; then
			if ! $patch_printed; then
				echo $patch:
				patch_printed=true
			fi
			$LIST_FILES && echo "   $file"
			(( count++ ))
		fi
	done
	$patch_printed && echo "   total: $count files"
done

# if inverse list hasn't been requested, exit
$INVERSE || exit

# print changed files and the patchs that touch them
echo -e "\nBY FILE:"
for file in $(quilt files --combine "$first_patch" "$last_patch" | sort -u); do
	if ! is_ignored_path "$file"; then
		echo $file:
		for patch in $(quilt patches "$file"); do
			echo "   $patch"
		done
	fi
done
