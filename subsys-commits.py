#!/usr/bin/python3
# SPDX-License-Identifier: GPL-2.0-or-later
# (c) 2022 Red Hat, Inc.; author: Jiri Benc <jbenc@redhat.com>
import argparse
import configparser
import copy
import fnmatch
import itertools
import json
import os
import sys
import textwrap
import urllib.error, urllib.parse, urllib.request
import yaml
from typing import Iterable

class GeneralError(Exception):
    pass


class Version(tuple):
    def __new__(cls, strver, prefix='rhel-', strict=True):
        if strict:
            if not strver:
                raise ValueError('Empty version specified')
            if not str.isnumeric(strver[0]) and not strver.startswith(prefix):
                raise ValueError("Expecting a bare version or a version starting with '{}', got '{}'"
                                 .format(prefix, strver))
        if strver.startswith(prefix):
            strver = strver[len(prefix):]
        data = []
        for k, g in itertools.groupby(strver, str.isnumeric):
            g = ''.join(g)
            if k:
                data.append(int(g))
                continue
            if strict and g != '.':
                raise ValueError("Expecting dot (.) as the version component separator in '{}', got '{}'"
                                 .format(strver, g))
        obj = tuple.__new__(cls, data)
        obj.prefix = prefix
        return obj

    def bare(self):
        return '.'.join(map(str, self))

    def __str__(self):
        return self.prefix + self.bare()


class Owners:
    class Error(GeneralError):
        pass

    def __init__(self, path=None):
        if path:
            try:
                f = open(path, 'r')
            except FileNotFoundError:
                raise self.Error('Cannot open {}'.format(path))
        else:
            try:
                f = urllib.request.urlopen('https://gitlab.com/redhat/centos-stream/src/kernel/documentation/-/raw/main/info/owners.yaml?inline=false')
            except urllib.error.HTTPError as e:
                raise self.Error('Cannot download owners.yaml: {}'.format(e))
        try:
            self.owners = yaml.safe_load(f)
        except yaml.YAMLError as e:
            raise self.Error('Cannot parse owners.yaml: {}'.format(e))

    def _get_subsystem(self, label):
        result = [entry for entry in self.owners['subsystems']
                        if entry.get('labels', {}).get('name') == label]
        if not result:
            raise self.Error('Subsystem "{}" not found'.format(label))
        return result

    def _all_path_sections(self, label):
        for entry in self._get_subsystem(label):
            if entry.get('paths'):
                yield entry['paths']

    def _glob_to_regex(self, pattern):
        # cannot use fnmatch.translate, since the returned regex cannot be
        # used with Kerneloscope
        result = []
        group = None
        for p in pattern:
            if group is not None:
                if p == ']':
                    if not group:
                        raise self.Error('Cannot translate shell pattern "{}": empty groups not supported'
                                         .format(pattern))
                    result.append('[{}]'.format(''.join(group)))
                    group = None
                    continue
                if not p.isalnum() and p not in ('.', '_', '-'):
                    raise self.Error('Cannot translate shell pattern "{}": unknown character "{}" in a group'
                                     .format(pattern, p))
                group.append(p)
            elif p == '[':
                group = []
            elif p == '?':
                result.append('.')
            elif p == '*':
                result.append('[^/]*')
            elif p == '.':
                result.append('\\.')
            elif not p.isalnum() and p not in ('_', '-', '/'):
                raise self.Error('Cannot translate shell pattern "{}": unknown character "{}"'
                                 .format(pattern, p))
            else:
                result.append(p)
        result = ''.join(result)
        if result.endswith('/'):
            return '^{}'.format(result)
        return '^{}$'.format(result)

    def _get_includes(self, subsys):
        result = set()
        for paths in self._all_path_sections(subsys):
            if paths.get('includes'):
                result.update(self._glob_to_regex(i) for i in paths['includes'])
            if paths.get('includeRegexes'):
                result.update(paths['includeRegexes'])
        return result

    def _get_excludes(self, subsys):
        result = set()
        for paths in self._all_path_sections(subsys):
            if paths.get('excludes'):
                result.update(self._glob_to_regex(i) for i in paths['excludes'])
        return result

    def _get_paths(self, subsys_list, callback):
        result = set()
        for subsys in subsys_list:
            result.update(callback(subsys))
        return '|'.join(result) or None

    def get_includes(self, subsys_list):
        return self._get_paths(subsys_list, self._get_includes)

    def get_excludes(self, subsys_list):
        return self._get_paths(subsys_list, self._get_excludes)


class Kerneloscope:
    server = 'kerneloscope.usersys.redhat.com'

    class Error(GeneralError):
        pass

    def call(self, name, **kwargs):
        filtered_args = { k: v for k, v in kwargs.items() if v is not None }
        data = json.dumps(filtered_args).encode('utf-8')
        req = urllib.request.Request('http://{}/rpc/{}/'.format(self.server, name), data=data,
                                     method='POST')
        req.add_header('Content-Type', 'application/json')
        try:
            data = json.load(urllib.request.urlopen(req))
        except urllib.error.URLError as e:
            raise self.Error('Cannot connect to Kerneloscope. Are you on Red Hat VPN? Error: {}'
                             .format(e))
        except urllib.error.HTTPError as e:
            raise self.Error('Kerneloscope server returned error: {}'.format(e))
        if 'error' in data:
            raise self.Error('Kerneloscope server returned error: {}'.format(data['error']))
        if 'result' not in data:
            raise self.Error('Unknown Kerneloscope server response')
        return data['result']

    def call_list(self, name, next_field='commit', **kwargs):
        result = []
        while True:
            data = self.call(name, **kwargs)
            if not data:
                break
            result.extend(data)
            kwargs['next'] = data[-1][next_field]
        return result

    def fetch_latest_rhel(self):
        versions = []
        for tree in self.call('get_trees'):
            if tree['type'] != 'RHEL' or not tree['name'].startswith('rhel-'):
                continue
            versions.append(Version(tree['name']))
        return max(versions)


class Backport:
    def __init__(self, kscope: Kerneloscope, owners: Owners, rhel: Version|str,
                 upstream: Version|str, excluded_commits: Iterable[str]):
        self.kscope = kscope
        self.owners = owners
        self.rhel = rhel
        self.upstream = upstream
        self.excluded_commits = excluded_commits

    def fetch(self, label):
        self.commit_info = {}
        self.commits = self._fetch_series(self._fetch_base_commits(label))
        self._fetch_fixes(self.commits)

    def is_excluded_commit(self, sha):
        return any(sha.startswith(exc) for exc in self.excluded_commits)

    def _fetch_base_commits(self, label):
        includes = self.owners.get_includes(label)
        excludes = self.owners.get_excludes(label)
        extra = {}
        if self.upstream:
            extra['top'] = str(self.upstream)
        commits = self.kscope.call_list('commit_list',
                                        tree_special='UPSTREAM', notin=str(self.rhel),
                                        path=includes, excl=excludes, options=['partial'],
                                        commit_with_subject=True,
                                        **extra)
        base_commits = []
        for c in commits:
            if c['merge']:
                continue
            sha = c['commit']
            if self.is_excluded_commit(sha):
                continue
            info = self.commit_info.setdefault(sha, { 'source': 'path' })
            for key in ('subject', 'trees', 'in_series', 'is_partial', 'is_reverted'):
                info[key] = c[key]
            base_commits.append(sha)
        return base_commits

    def _fetch_series(self, base_commits):
        commits = self.kscope.call('get_missing_series', tree=str(self.rhel),
                                   commits=base_commits, commit_with_subject=True)
        series_commits = []
        for c in commits:
            if c['merge'] or (c['included'] and c['added']):
                # Ignore merge commits and already backported commits. Note
                # that partially backported commits have included: True; we
                # skip the partial backports from the rest of the series but
                # we want to keep partial backports that are in base_commits
                # (i.e. have added: False).
                continue
            sha = c['commit']
            if self.is_excluded_commit(sha):
                continue
            info = self.commit_info.setdefault(sha, { 'source': 'series' })
            for key in ('subject', 'trees', 'series'):
                info[key] = c[key]
            series_commits.append(sha)
        return series_commits

    def _fetch_fixes(self, base_commits):
        kind_map = { '': 'mention', 'f': 'fix', 'r': 'revert', 'i': None }
        # Need to include 'i' to prevent crashes when resolving the 'kind'
        # field. However, 'i' will never get used, since the commit is
        # already in self.commit_info.

        commits = self.kscope.call('get_missing_fixes', tree=str(self.rhel),
                                   commits=base_commits, commit_with_subject=True)
        for c in commits:
            if c['merge']:
                continue
            sha = c['commit']
            if self.is_excluded_commit(sha):
                continue
            info = self.commit_info.setdefault(sha, { 'source': kind_map[c['kind']] })
            for key in ('subject', 'trees', 'fixes'):
                info[key] = c[key]

    def get_source_flag(self, info):
        # It doesn't make sense to flag reverts. A revert is generally
        # touching the same paths as the original commit. That means that
        # reverts of the base commits will be included in the base commit
        # list. The only things marked as revert would thus be reverts of
        # a follow up fixes. And those are close to the commits they revert.
        # For consistency, don't mark anything.
        if info.get('is_partial'):
            return '%'
        return { 'path': '*', 'series': ' ', 'fix': ' ', 'revert': ' ',
                 'mention': '?' }[info['source']]

    def get_trees(self, info):
        if 'linux' in info['trees']:
            return ''
        return '(in {}) '.format(', '.join(info['trees']))

    def __iter__(self):
        for i, sha in enumerate(self.commits):
            info = self.commit_info[sha]

            # get series flag
            start = (i == 0 or
                     self.commit_info[self.commits[i - 1]]['series'] != info['series'])
            end = (i == len(self.commits) - 1 or
                   self.commit_info[self.commits[i + 1]]['series'] != info['series'])
            sflag = '│┐┘ '[int(start) + 2 * int(end)]
            sflag2 = '⋮⋮  '[int(start) + 2 * int(end)]

            text = '{:.12} {}{} {}{}'.format(sha, self.get_source_flag(info), sflag,
                                             self.get_trees(info), info['subject'])
            yield text

            fixes = [fix for fix in info.get('fixes', ()) if fix in self.commit_info]
            # merge commits do not have an entry in commit_info
            for i, fix in enumerate(fixes):
                finfo = self.commit_info[fix]
                fflag = '└' if i == len(fixes) - 1 else '├'
                text = '{:.12} {}{} {}─ {}{}'.format(fix, self.get_source_flag(finfo),
                                                     sflag2, fflag, self.get_trees(finfo),
                                                     finfo['subject'])
                yield text

    def print(self, cols=None):
        for s in self:
            print(s[:cols])


class Config:
    class Error(GeneralError):
        pass

    def __init__(self):
        self.excluded = configparser.ConfigParser(allow_no_value=True,
                                                  default_section=None)
        self.excluded.read((self._get_excluded_path(),))

    def _get_config_dir(self):
        return os.environ.get('XDG_CONFIG_HOME') or os.path.expanduser('~/.config')

    def _get_excluded_path(self, create_dir=False):
        dirname = self._get_config_dir()
        if create_dir:
            os.makedirs(dirname, exist_ok=True)
        return os.path.join(dirname, 'subsys-commits.excludes')

    def save(self):
        fname = self._get_excluded_path(create_dir=True)
        try:
            f = open(fname, 'w')
        except OSError as e:
            raise self.Error(str(e)) from None
        try:
            self.excluded.write(f)
        finally:
            f.close()

    def get_excluded(self, rhel):
        commits = set()
        for sect in self.excluded.sections():
            if sect.startswith('rhel-'):
                v = Version(sect)
                if v[0] != rhel[0] or v > rhel:
                    continue
            elif sect != 'all':
                continue
            for k in self.excluded[sect].keys():
                commits.add(k)
        return commits


class App:
    class ArgContParser(argparse.ArgumentParser):
        # exit_on_error is available only starting with Python 3.9; but even
        # in 3.10, it's useless, since the parsing still calls error() on
        # certain checks, most importantly on missing required argument. We need
        # to do our own handling.

        class ArgError(Exception):
            pass

        def error(self, msg, *args, force=False, **kwargs):
            if not force:
                raise self.ArgError(msg)
            super().error(msg, *args, **kwargs)

    def __init__(self, argv):
        self.parse_args(argv)
        try:
            self.cols = os.get_terminal_size()[0] - 1
        except OSError:
            self.cols = None
        self.config = Config()

    def parse_args(self, argv):
        def print_help(file=None):
            file = file or sys.stdout
            for p in parsers:
                p.print_usage(file)
            file.write('\n')
            all_parser.print_help(file)

        help_parser = self.ArgContParser(add_help=False)
        help_parser.add_argument('--help', '-h', required=True, action='store_true',
                                 help='show this help message and exit')

        exclude_parser = self.ArgContParser(add_help=False)
        exclude_parser.add_argument('--exclude', '-e', required=True, action='store_true',
                                    help='add the commit(s) to the exclude list; with --rhel, limit the exclusion to the given RHEL version and newer versions')
        exclude_parser.add_argument('--rhel', '-r', metavar='VERSION',
                                    help='RHEL version to backport against')
        exclude_parser.add_argument('commit', nargs='+',
                                    help='commit sha to add to the exclude list')

        main_parser = argparse.ArgumentParser(add_help=False)
        main_parser.add_argument('label', nargs='+', help='a subsystem label')
        main_parser.add_argument('--rhel', '-r', metavar='VERSION',
                                 help='RHEL version to backport against')
        main_parser.add_argument('--upstream', '-u', metavar='VERSION',
                                 help='commits only up to the upstream version')
        main_parser.add_argument('--owners', metavar='FILE', help='use a custom owners.yaml')

        parsers = (main_parser, exclude_parser, help_parser)
        more_help = textwrap.dedent('''
        The commits displayed are marked with these flags:
          *  commit that directly touches the given subsystem(s)
          ?  commit that mentions another included commit; it may or may not be needed
          %  commit that was previously partially backported''')
        all_parser = argparse.ArgumentParser(description='Return the commits to be backported for the given subsystem(s).',
                                             usage=argparse.SUPPRESS,
                                             epilog=more_help,
                                             formatter_class=argparse.RawDescriptionHelpFormatter,
                                             parents=tuple(copy.deepcopy(p) for p in parsers),
                                             add_help=False, conflict_handler='resolve')

        # output help if there are no arguments:
        if not argv:
            print_help()
            sys.exit(0)
        # check for --help:
        try:
            self.args = help_parser.parse_known_args(argv)
            print_help()
            sys.exit(0)
        except self.ArgContParser.ArgError:
            pass
        # check for --exclude:
        try:
            self.args = exclude_parser.parse_known_args(argv)
            is_exclude = True
        except self.ArgContParser.ArgError as e:
            is_exclude = 'exclude' not in str(e)
        if is_exclude:
            # reparse with error checking
            try:
                self.args = exclude_parser.parse_args(argv)
            except self.ArgContParser.ArgError as e:
                exclude_parser.error(str(e), force=True)
            self.command = 'exclude'
            return
        self.args = main_parser.parse_args(argv)
        self.command = 'main'
        return

    def sanitize_args(self):
        if self.args.rhel:
            self.rhel = Version(self.args.rhel, strict=False)
        else:
            self.rhel = self.kscope.fetch_latest_rhel()
            sys.stderr.write('WARNING: no --rhel specified, using {}\n'
                             .format(self.rhel.bare()))
        try:
            self.upstream = Version(self.args.upstream, prefix='v')
        except ValueError:
            # no --upstream, or a non-version specified as --upstream; treat
            # the latter as a commit hash
            self.upstream = self.args.upstream

    def run_main(self):
        owners = Owners(self.args.owners)
        self.kscope = Kerneloscope()
        self.sanitize_args()

        backport = Backport(self.kscope, owners, self.rhel, self.upstream,
                            self.config.get_excluded(self.rhel))
        backport.fetch(self.args.label)
        backport.print(self.cols)

    def run_exclude(self):
        if self.args.rhel:
            self.rhel = Version(self.args.rhel, strict=False)
            sect = str(self.rhel)
        else:
            sect = 'all'
        try:
            self.config.excluded.add_section(sect)
        except configparser.DuplicateSectionError:
            pass
        for c in self.args.commit:
            self.config.excluded[sect][c] = None
        self.config.save()

    def run(self):
        getattr(self, 'run_' + self.command)()


if __name__ == '__main__':
    try:
        app = App(sys.argv[1:])
        app.run()
    except GeneralError as e:
        sys.stderr.write('{}\n'.format(e))
        sys.exit(1)
